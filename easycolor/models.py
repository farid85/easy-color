from django.db import models
from django.utils.safestring import mark_safe

# Create your models here.
class Color(models.Model):
    color = models.CharField(blank=True, max_length=500)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.color

    def back_colored(self):
        return mark_safe('<h2 height="50" width="50" style="background:{};">{}</h2>'.format(self.color, self.color))

class Category(models.Model):
    colors = models.ManyToManyField(Color)
    category = models.CharField(blank=True, max_length=500)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.category


class Setting(models.Model):
    title = models.CharField(blank=True, max_length=500)
    description = models.TextField(blank=True, max_length=5000)
    url = models.URLField(blank=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
